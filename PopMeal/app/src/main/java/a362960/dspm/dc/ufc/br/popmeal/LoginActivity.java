package a362960.dspm.dc.ufc.br.popmeal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText nomeUsuarioT,senhaT;
    String nomeUsuario,senha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    }


    //Chama tela principal-----------------------------------------------------
    public void TelaPrincipal(View view){
        nomeUsuarioT = ((EditText)findViewById(R.id.userT));
        senhaT = ((EditText)findViewById(R.id.senha));
        if(Client.verificar(nomeUsuarioT.getText().toString(),senhaT.getText().toString(),this)==0){
            Intent intent = new Intent(this,MainActivity.class);
            nomeUsuarioT = ((EditText)findViewById(R.id.userT));
            senhaT = ((EditText)findViewById(R.id.senha));
            intent.putExtra("Usuario",nomeUsuarioT.getText().toString());
            intent.putExtra("Senha",senhaT.getText().toString());
            startActivity(intent);
        }
        else{
            Toast.makeText(this, "Senha ou usuario errados", Toast.LENGTH_SHORT).show();
            limpar();
        }
    }

    //Chama tela principal-----------------------------------------------------



    //Limpa os edittext caso o usuario e a senha estejam incorretos---------------------
    public void limpar(){
        nomeUsuarioT.setText("");
        senhaT.setText("");
    }
    //Limpa os edittext caso o usuario e a senha estejam incorretos---------------------




    //Chamar tela para recuperar senha-----------------------------
    public void esqueceuSenha(View view){
        Intent intent = new Intent(this,ForgotPasswordActivity.class);
        startActivity(intent);
    }
    //Chamar tela para recuperar senha-----------------------------


    //Chama tela para cadastrar usuário----------------------------
    public void TelaCadastro(View view){
        Intent intent = new Intent(this,RegisterActivity.class);
        startActivity(intent);
    }
    //Chama tela para cadastrar usuário----------------------------
}
