package a362960.dspm.dc.ufc.br.popmeal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {
    EditText usuario,senha,senhaconfirmar,email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        usuario = ((EditText)findViewById(R.id.user));
        senha = ((EditText)findViewById(R.id.senha));
        email = ((EditText)findViewById(R.id.email));
        senhaconfirmar = ((EditText)findViewById(R.id.senhaconfirmar));
    }
    public void TelaLogin(View view){
        Intent intent = new Intent(this,LoginActivity.class);
        usuario = ((EditText)findViewById(R.id.user));
        senha = ((EditText)findViewById(R.id.senha));
        email = ((EditText)findViewById(R.id.email));
        intent.putExtra("Usuario",usuario.getText().toString());
        intent.putExtra("Senha",senha.getText().toString());
        intent.putExtra("Usuario",email.getText().toString());
        if(!senhaconfirmar.getText().toString().equals(senha.getText().toString())) {
            Toast.makeText(this, "Senhas não são compatíveis", Toast.LENGTH_SHORT).show();
            limpar();
        }
        else{
            Client.add(usuario.getText().toString(),email.getText().toString(),senha.getText().toString());
            startActivity(intent);
            finish();

        }
//        startActivity(intent);
//        finish();
    }
    public void limpar(){
//        usuario.setText("");
        senha.setText("");
        senhaconfirmar.setText("");
//        email.setText("");
    }
}
