package a362960.dspm.dc.ufc.br.popmeal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ForgotPasswordActivity extends AppCompatActivity {

    EditText usuario,senha,senhaconfirmar,email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }
    public void TelaLogin(View view){
        Intent intent = new Intent(this,LoginActivity.class);
        email = ((EditText)findViewById(R.id.email));
        intent.putExtra("email",email.getText().toString());
        Toast.makeText(this, "Será enviado um link de recuperação ao seu email", Toast.LENGTH_SHORT).show();
        startActivity(intent);
        finish();

    }
    @Override
    protected void onResume() {
        super.onResume();

    }
}
